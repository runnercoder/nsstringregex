# NSStringRegex

[![CI Status](http://img.shields.io/travis/starrykai/NSStringRegex.svg?style=flat)](https://travis-ci.org/starrykai/NSStringRegex)
[![Version](https://img.shields.io/cocoapods/v/NSStringRegex.svg?style=flat)](http://cocoapods.org/pods/NSStringRegex)
[![License](https://img.shields.io/cocoapods/l/NSStringRegex.svg?style=flat)](http://cocoapods.org/pods/NSStringRegex)
[![Platform](https://img.shields.io/cocoapods/p/NSStringRegex.svg?style=flat)](http://cocoapods.org/pods/NSStringRegex)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NSStringRegex is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "NSStringRegex"
```

## Author

starrykai, starryskykai@gmail.com

## License

NSStringRegex is available under the MIT license. See the LICENSE file for more info.
