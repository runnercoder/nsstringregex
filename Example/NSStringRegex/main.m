//
//  main.m
//  NSStringRegex
//
//  Created by starrykai on 09/07/2016.
//  Copyright (c) 2016 starrykai. All rights reserved.
//

@import UIKit;
#import "MallAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MallAppDelegate class]));
    }
}
