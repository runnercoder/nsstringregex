//
//  NSString+AppStringCategory.h
//  Tao
//
//  Created by 吴恺 on 15/8/5.
//  Copyright (c) 2015年 wukai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (RegexCheck)

/**
 *  检查字符串是否为邮箱格式
 *
 *  @return YES or NO
 */
- (BOOL)isValidateEmail;

/**
 *  检查字符串是否为手机号 （11位数字检测）
 *
 *  @return YES or NO
 */
- (BOOL)isValidatePhone;
@end
