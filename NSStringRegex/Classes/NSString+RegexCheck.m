//
//  NSString+AppStringCategory.m
//  Tao
//
//  Created by 吴恺 on 15/8/5.
//  Copyright (c) 2015年 wukai. All rights reserved.

#import "NSString+RegexCheck.h"

@implementation NSString (RegexCheck)

// check email
- (BOOL)isValidateEmail {
  NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
  return [self validateDataWithRegex:emailRegex];
}

// check china mobile phone number
- (BOOL)isValidatePhone {
  NSString *phoneRegex =
      @"^1(3[0-9]|4[57]|5[0-35-9]|(7[0[059]|6｜7｜8])|8[0-9])\\d{8}$";
  return [self validateDataWithRegex:phoneRegex];
}

// check value with regex
- (BOOL)validateDataWithRegex:(NSString *)regex {
  NSPredicate *predicate =
      [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
  return [predicate evaluateWithObject:self];
}

@end
